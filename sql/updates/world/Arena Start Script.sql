-- Fix Shadowmeld.
DELETE FROM spell_script_names WHERE spell_id = 58984;
INSERT INTO spell_script_names VALUES (58984, 'spell_gen_shadowmeld');

-- fast arena start
 -- Implement Fast Arena Start
 SET @GGUID := 999999;
 DELETE FROM `gameobject_template` WHERE `entry` = 42000;
INSERT INTO `gameobject_template` VALUES (42000, 10, 327, 'Arena Crystal', 'PVP', '', '', 0, 0, 1.5, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 'fast_arena_start', 12340);
DELETE FROM `gameobject` WHERE  `id`=42000;
INSERT INTO `gameobject` VALUES (@GGUID+1, 42000, 562, 1, 1, 6287.79, 288.368, 5.26778, 3.91128, 0, 0, 0.926857, -0.375416, 300, 0, 1);
INSERT INTO `gameobject` VALUES (@GGUID+2, 42000, 562, 1, 1, 6188.78, 235.324, 5.29393, 0.891424, 0, 0, 0.431101, 0.902304, 300, 0, 1);
INSERT INTO `gameobject` VALUES (@GGUID+3, 42000, 559, 1, 1, 4090.01, 2873.47, 12.1158, 2.08915, 0, 0, 0.864712, 0.502267, 300, 0, 1);
INSERT INTO `gameobject` VALUES (@GGUID+4, 42000, 572, 1, 1, 1297.43, 1597.66, 31.6135, 4.88125, 0, 0, 0.644959, -0.764217, 300, 0, 1);
INSERT INTO `gameobject` VALUES (@GGUID+5, 42000, 572, 1, 1, 1274.79, 1732.94, 31.6048, 4.81449, 0, 0, 0.670103, -0.742268, 300, 0, 1);
INSERT INTO `gameobject` VALUES (@GGUID+6, 42000, 617, 1, 1, 1352.68, 815.687, 15.2511, 0.401281, 0, 0, 0.199297, 0.979939, 300, 0, 1);
INSERT INTO `gameobject` VALUES (@GGUID+7, 42000, 617, 1, 1, 1229.86, 761.566, 15.7332, 0.0471227, 0, 0, 0.0235592, 0.999722, 300, 0, 1);
INSERT INTO `gameobject` VALUES (@GGUID+8, 42000, 559, 1, 1, 4023.85, 2967.25, 12.1642, 5.05796, 0, 0, 0.575005, -0.81815, 300, 0, 1);
