#include "ScriptPCH.h"
#include "Chat.h"

uint32 bufove[] = { 48162, 48074, 48170, 43223, 36880, 467, 48469 };

class buffkomanda : public CommandScript
{
public:
    buffkomanda() : CommandScript("buffkomanda") { }

	ChatCommand* GetCommands() const
    {
        static ChatCommand IngameCommandTable[] =
        {
	{ "buff",           SEC_PLAYER,  	true,  &Handlebuffkomanda,                "", NULL },
        { NULL,             0,                  false, NULL,                              "", NULL }
        };
		 return IngameCommandTable;
    }

	static bool Handlebuffkomanda(ChatHandler * handler, const char * args)
    {
        Player * pl = handler->GetSession()->GetPlayer();
		if(pl->InArena())
		{
			pl->GetSession()->SendNotification("Ne mojesh da izpolzvash bufovete kogato si v 2v2,3v3,5v5 areni!");
			return false;
		}
	
		pl->RemoveAurasByType(SPELL_AURA_MOUNTED);
		for(int i = 0; i < 11; i++)
		    pl->AddAura(bufove[i], pl);
		handler->PSendSysMessage("|cffB400B4Ti si buff-nat uspeshno, priqtna igra!");
		return true;

    }
};

void AddSC_buffkomanda()
{
    new buffkomanda();
}