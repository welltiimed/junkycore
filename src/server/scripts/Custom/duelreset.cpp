#include "ScriptPCH.h"

class Duel_Reset : public PlayerScript
{
	public:
		Duel_Reset() : PlayerScript("Duel_Reset"){}
	/* Nachalo na duela */
	void PlayerDuelRequest(Player* predizvikatel, Player* igrach, DuelCompleteType /*type*/)
	{
				predizvikatel->RemoveAllSpellCooldown();
				igrach->RemoveAllSpellCooldown();
				predizvikatel->SetHealth(predizvikatel->GetMaxHealth());
				igrach->SetHealth(igrach->GetMaxHealth());
				igrach->SetPower(POWER_MANA, igrach->GetMaxPower(POWER_MANA));
				predizvikatel->SetPower(POWER_MANA, predizvikatel->GetMaxPower(POWER_MANA));
			
	}
	/* Krai na duel-a */
	void OnDuelEnd(Player* pobeditel, Player* zagubil, DuelCompleteType /*type*/)
	{
				pobeditel->RemoveAllSpellCooldown();
				zagubil->RemoveAllSpellCooldown();
				pobeditel->SetHealth(pobeditel->GetMaxHealth());
				zagubil->SetHealth(zagubil->GetMaxHealth());
				pobeditel->SetPower(POWER_MANA, pobeditel->GetMaxPower(POWER_MANA));
				zagubil->SetPower(POWER_MANA, zagubil->GetMaxPower(POWER_MANA));
	}
};

void AddSC_Duel_Reset()
{
	new Duel_Reset();
}