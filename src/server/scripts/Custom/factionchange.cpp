#include "ScriptPCH.h"
 
class npc_changefaction : public CreatureScript
{
public:
    npc_changefaction() : CreatureScript("npc_changefaction") { }
 
    bool OnGossipHello(Player *_player, Creature *_creature)
    {
	/*
  	if (_player->GetTeam() != ALLIANCE)
		{
			_player->CLOSE_GOSSIP_MENU();
			_creature->MonsterWhisper("You must be Alliance!", _player->GetGUID());
			return true;
			
		*/	
		if (_player->isInCombat())
			{
				_player->CLOSE_GOSSIP_MENU();
				_creature->MonsterWhisper("Combat!", _player->GetGUID());
				return true;
			}
			
		
        else
        {
            _player->ADD_GOSSIP_ITEM(3,"|cffFFFF00I want to change my faction|r [|cff00FF00FREE|r]"                       , GOSSIP_SENDER_MAIN, 1);
		    _player->ADD_GOSSIP_ITEM( 1, "|cffFFFF00I want to Rename my character|r [|cff00FF00FREE|r]"                   , GOSSIP_SENDER_MAIN, 3);
            _player->ADD_GOSSIP_ITEM( 6, "|cffFFFF00I want a Character Customization|r [|cff00FF00FREE|r]"               , GOSSIP_SENDER_MAIN, 4);
			_player->ADD_GOSSIP_ITEM( 6, "|cffFFFF00I want a Race Change|r [|cff00FF00FREE|r]"               , GOSSIP_SENDER_MAIN, 5);
            _player->ADD_GOSSIP_ITEM(9,"|cff667700I'd prefer to stay on this faction!|r"                  , GOSSIP_SENDER_MAIN, 2);
			
            
        }
 
        _player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
        return true;
    }
 
    bool OnGossipSelect(Player *_player, Creature *_creature, uint32 sender, uint32 uiAction)
    {
        if (sender == GOSSIP_SENDER_MAIN)
        {
            _player->PlayerTalkClass->ClearMenus();
            switch(uiAction)
            {
			
      case 1://Case - Faction Change
          _player->CLOSE_GOSSIP_MENU();
          _player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
		  _creature->MonsterWhisper("You have been flagged for faction change, |cffFF0000Relog|r!", _player->GetGUID());
      break;
	  
      case 2://Case - Exit
          _player->CLOSE_GOSSIP_MENU();
      break;
 
       case 3://Case - Rename
          _player->CLOSE_GOSSIP_MENU();
          _player->SetAtLoginFlag(AT_LOGIN_RENAME);
		  _creature->MonsterWhisper("You have been flagged for character rename, |cffFF0000Relog|r!", _player->GetGUID());
      break;
	  
      case 4://Case - Customization
          _player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
		  _creature->MonsterWhisper("You have been flagged for character recustomization, |cffFF0000Relog|r!", _player->GetGUID());
          _player->CLOSE_GOSSIP_MENU();
      break;
	  
	   case 5://Case - Race
          _player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
		  _creature->MonsterWhisper("You have been flagged for race change, |cffFF0000Relog|r!", _player->GetGUID());
          _player->CLOSE_GOSSIP_MENU();
      break;
             default:
        break;                   
    }
   }
  return true;
 }
};
 
void AddSC_npc_changefaction()
{
    new npc_changefaction();
}
