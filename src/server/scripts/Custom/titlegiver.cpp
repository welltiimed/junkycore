#include "ScriptPCH.h"

class npc_title : public CreatureScript
{
public:
    npc_title() : CreatureScript("npc_title") { }

    bool OnGossipHello(Player *player, Creature *_creature)
    {
        if (player->isInCombat())
        {
            player->CLOSE_GOSSIP_MENU();
            _creature->MonsterWhisper("Combat!", player->GetGUID());
            return true;
        }
        else
        {
            player->ADD_GOSSIP_ITEM( 3, "General"                       , GOSSIP_SENDER_MAIN, 1);
            player->ADD_GOSSIP_ITEM( 8, "Event"                                 , GOSSIP_SENDER_MAIN, 2);
            player->ADD_GOSSIP_ITEM( 1, "Dungeon & Raid"                     , GOSSIP_SENDER_MAIN, 3);
             player->ADD_GOSSIP_ITEM( 6, "Reputation"                           , GOSSIP_SENDER_MAIN, 4);
             player->ADD_GOSSIP_ITEM( 9, "PvP & Arena"                  , GOSSIP_SENDER_MAIN, 5);
             player->ADD_GOSSIP_ITEM( 10, "Other"                              , GOSSIP_SENDER_MAIN, 6);
        }

        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
        return true;
    }
    bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 uiAction)
    {
        if (sender == GOSSIP_SENDER_MAIN)
        {
            player->PlayerTalkClass->ClearMenus();
            switch(uiAction)
            {
                         case 1: //General
                                player->ADD_GOSSIP_ITEM( 0, "%s The Explorer"                      , GOSSIP_SENDER_MAIN, 8);
                                player->ADD_GOSSIP_ITEM( 0, "Chef %s"                              , GOSSIP_SENDER_MAIN, 9);
                                player->ADD_GOSSIP_ITEM( 0, "Salty %s"                             , GOSSIP_SENDER_MAIN, 10);
                                player->ADD_GOSSIP_ITEM( 0, "Loremaster %s"                        , GOSSIP_SENDER_MAIN, 11);
                                player->ADD_GOSSIP_ITEM( 0, "%s The Seeker"                        , GOSSIP_SENDER_MAIN, 12);
                                player->ADD_GOSSIP_ITEM( 0, "%s The Patient"                       , GOSSIP_SENDER_MAIN, 13);

                                player->ADD_GOSSIP_ITEM( 7, "<- [Main Menu]"                            , GOSSIP_SENDER_MAIN, 7);

                                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
                               break;

                         case 2: //Event
                                player->ADD_GOSSIP_ITEM( 0, "Elder"                             , GOSSIP_SENDER_MAIN, 15);
                                player->ADD_GOSSIP_ITEM( 0, "The Love Fool"                     , GOSSIP_SENDER_MAIN, 16);
                                player->ADD_GOSSIP_ITEM( 0, "The Noble"                         , GOSSIP_SENDER_MAIN, 17);
                                player->ADD_GOSSIP_ITEM( 0, "Brewmaster"                        , GOSSIP_SENDER_MAIN, 18);
                                player->ADD_GOSSIP_ITEM( 0, "The Hallowed"                      , GOSSIP_SENDER_MAIN, 19);
                                player->ADD_GOSSIP_ITEM( 0, "Pilgrim"                                   , GOSSIP_SENDER_MAIN, 20);
                                player->ADD_GOSSIP_ITEM( 0, "Merrymaker"                        , GOSSIP_SENDER_MAIN, 21);

                                player->ADD_GOSSIP_ITEM( 7, "<- [Main Menu]"                            , GOSSIP_SENDER_MAIN, 7);

                                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
                               break;

                         case 3: //Dungeon & Raid
                                player->ADD_GOSSIP_ITEM( 0, "Jenkins"                                   , GOSSIP_SENDER_MAIN, 22);
                                player->ADD_GOSSIP_ITEM( 0, "Scarab Lord"                       , GOSSIP_SENDER_MAIN, 23);
                                player->ADD_GOSSIP_ITEM( 0, "Champion of the Frozen Wastes"   , GOSSIP_SENDER_MAIN, 24);
                                player->ADD_GOSSIP_ITEM( 0, "The Undying"                       , GOSSIP_SENDER_MAIN, 25);
                                player->ADD_GOSSIP_ITEM( 0, "The Immortal"                      , GOSSIP_SENDER_MAIN, 26);
                                player->ADD_GOSSIP_ITEM( 0, "of the Nightfall"                  , GOSSIP_SENDER_MAIN, 27);
                                player->ADD_GOSSIP_ITEM( 0, "Twilight Vanquisher"               , GOSSIP_SENDER_MAIN, 28);
                                player->ADD_GOSSIP_ITEM( 0, "Starcaller"                        , GOSSIP_SENDER_MAIN, 29);
                                player->ADD_GOSSIP_ITEM( 0, "The Astral Walker"                 , GOSSIP_SENDER_MAIN, 30);
                                player->ADD_GOSSIP_ITEM( 0, "Herald of the Titans"              , GOSSIP_SENDER_MAIN, 31);
                                player->ADD_GOSSIP_ITEM( 0, "Champion of Ulduar"                , GOSSIP_SENDER_MAIN, 32);
                                player->ADD_GOSSIP_ITEM( 0, "Conqueror of Ulduar"               , GOSSIP_SENDER_MAIN, 33);
                                player->ADD_GOSSIP_ITEM( 0, "Bane of the Fallen King"           , GOSSIP_SENDER_MAIN, 34);
                                player->ADD_GOSSIP_ITEM( 0, "The Light of Dawn"                 , GOSSIP_SENDER_MAIN, 35);
                                player->ADD_GOSSIP_ITEM( 0, "The Kingslayer"                    , GOSSIP_SENDER_MAIN, 36);

                                player->ADD_GOSSIP_ITEM( 7, "<- [Main Menu]"                            , GOSSIP_SENDER_MAIN, 7);

                                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
                               break;

                         case 4: //Reputation
                                player->ADD_GOSSIP_ITEM( 0, "of the Shattered Sun"              , GOSSIP_SENDER_MAIN, 37);
                                player->ADD_GOSSIP_ITEM( 0, "Ambassador"                        , GOSSIP_SENDER_MAIN, 38);
                                player->ADD_GOSSIP_ITEM( 0, "Bloodsail Admiral          "               , GOSSIP_SENDER_MAIN, 39);
                                player->ADD_GOSSIP_ITEM( 0, "The Diplomat"                      , GOSSIP_SENDER_MAIN, 40);
                                player->ADD_GOSSIP_ITEM( 0, "Guardian of Cenarius"              , GOSSIP_SENDER_MAIN, 41);
                                player->ADD_GOSSIP_ITEM( 0, "The Argent Champion"               , GOSSIP_SENDER_MAIN, 42);
                                player->ADD_GOSSIP_ITEM( 0, "of the Ashen Verdict"              , GOSSIP_SENDER_MAIN, 43);
                                player->ADD_GOSSIP_ITEM( 0, "The Exalted"                       , GOSSIP_SENDER_MAIN, 44);
                                player->ADD_GOSSIP_ITEM( 0, "The Insane"                        , GOSSIP_SENDER_MAIN, 45);

                                player->ADD_GOSSIP_ITEM( 7, "<- [Main Menu]"                            , GOSSIP_SENDER_MAIN, 7);

                                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
                               break;

                         case 5: //PvP & Arena
                                player->ADD_GOSSIP_ITEM( 0, "Battlemaster"              , GOSSIP_SENDER_MAIN, 46);
                                player->ADD_GOSSIP_ITEM( 0, "Arena Master"              , GOSSIP_SENDER_MAIN, 47);
                                player->ADD_GOSSIP_ITEM( 0, "Duelist"                           , GOSSIP_SENDER_MAIN, 48);
                                player->ADD_GOSSIP_ITEM( 0, "Rival"                     , GOSSIP_SENDER_MAIN, 49);
                                player->ADD_GOSSIP_ITEM( 0, "Challenger"                , GOSSIP_SENDER_MAIN, 50);
                                player->ADD_GOSSIP_ITEM( 0, "Vanquisher"                , GOSSIP_SENDER_MAIN, 51);

                                player->ADD_GOSSIP_ITEM( 7, "<- [Main Menu]"                    , GOSSIP_SENDER_MAIN, 7);

                                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
                               break;

                         case 6: //Other
                                player->ADD_GOSSIP_ITEM( 0, "Crusader"                          , GOSSIP_SENDER_MAIN, 52);
                                player->ADD_GOSSIP_ITEM( 0, "Archmage"                          , GOSSIP_SENDER_MAIN, 53);
                                player->ADD_GOSSIP_ITEM( 0, "Champion of the Naaru"             , GOSSIP_SENDER_MAIN, 54);
                                player->ADD_GOSSIP_ITEM( 0, "Hand of A'dal"                     , GOSSIP_SENDER_MAIN, 55);
                                player->ADD_GOSSIP_ITEM( 0, "Flawless Victor"                   , GOSSIP_SENDER_MAIN, 56);

                                player->ADD_GOSSIP_ITEM( 7, "<- [Main Menu]"                            , GOSSIP_SENDER_MAIN, 7);

                                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
                               break;

                         case 7: //<- [Main Menu]
                                player->ADD_GOSSIP_ITEM( 3, "General"                           , GOSSIP_SENDER_MAIN, 1);
                                player->ADD_GOSSIP_ITEM( 8, "Event"                             , GOSSIP_SENDER_MAIN, 2);
                                player->ADD_GOSSIP_ITEM( 1, "Dungeon & Raid"                   , GOSSIP_SENDER_MAIN, 3);
                                player->ADD_GOSSIP_ITEM( 6, "Reputation"                        , GOSSIP_SENDER_MAIN, 4);
                                player->ADD_GOSSIP_ITEM( 9, "PvP & Arena"                       , GOSSIP_SENDER_MAIN, 5);
                                player->ADD_GOSSIP_ITEM( 10, "Other"                           , GOSSIP_SENDER_MAIN, 6);

                                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());
                               break;

            case 8://The Explorer
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(78));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 9://Chef
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(84));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 10://Salty
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(83));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 11://Loremaster
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(125));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 12://The Seeker
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(81));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 13://The Patient
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(172));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 15://Elder
                if (player->HasItemCount( 32569, 25, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 25, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(74));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 16://The Love Fool
                if (player->HasItemCount( 32569, 25, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 25, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(135));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 17://The Noble
                if (player->HasItemCount( 32569, 25, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 25, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(155));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 18://Brewmaster
                if (player->HasItemCount( 32569, 25, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 25, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(133));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 19://The Hallowed
                if (player->HasItemCount( 32569, 25, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 25, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(124));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 20://Pilgrim
                if (player->HasItemCount( 32569, 25, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 25, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(168));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 21://Merrymaker
                if (player->HasItemCount( 32569, 25, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 25, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(134));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 22://Jenkins
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(143));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 23://Scarab Lord
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(46));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 24://Champion of the Frozen Wastes
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(129));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 25://The Undying
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(142));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 26://The Immortal
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(141));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 27://of the Nightfall
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(140));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 28://Twilight Vanquisher
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(121));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 29://Starcaller
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(164));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 30://The Astral Walker
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(165));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 31://Herald of the Titans
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(166));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 32://Champion of Ulduar
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(161));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 33://Conqueror of Ulduar
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(160));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 34://Bane of the Fallen King
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(174));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 35://The Light of Dawn
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(173));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 36://The Kingslayer
                if (player->HasItemCount( 32569, 0, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 0, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(175));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 37://of the Shattered Sun
                if (player->HasItemCount( 32569, 30, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 30, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(63));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 38://Ambassador
                if (player->HasItemCount( 32569, 30, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 30, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(130));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 39://Bloodsail Admiral
                if (player->HasItemCount( 32569, 30, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 30, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(144));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 40://The Diplomat
                if (player->HasItemCount( 32569, 30, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 30, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(79));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 41://Guardian of Cenarius
                if (player->HasItemCount( 32569, 30, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 30, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(132));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 42://The Argent Champion
                if (player->HasItemCount( 32569, 30, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 30, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(131));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 43://of the Ashen Verdict
                if (player->HasItemCount( 32569, 30, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 30, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(176));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 44://The Exalted
                if (player->HasItemCount( 32569, 30, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 30, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(77));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 45://The Insane
                if (player->HasItemCount( 32569, 30, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 30, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(145));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 46://Battlemaster
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(72));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 47://Arena Master
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(82));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 48://Duelist
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(43));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 49://Rival
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(44));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 50://Challenger
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(45));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 51://Vanquisher
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(163));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 52://Crusader
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(156));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 53://Archmage
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(93));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 54://Champion of the Naaru
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(53));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 55://Hand of A'dal
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(64));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;

            case 56://Flawless Victor
                if (player->HasItemCount( 32569, 60, false ))
                {
                    player->CLOSE_GOSSIP_MENU();
                    player->DestroyItemCount(32569, 60, true, false);
                    player->SetTitle(sCharTitlesStore.LookupEntry(128));
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Not Enough Tokens!", player->GetGUID());
                    return false;
                }
                break;
            default:
                break;             
            }
        }
        return true;
    }
};

void AddSC_npc_title()
{
    new npc_title();
}