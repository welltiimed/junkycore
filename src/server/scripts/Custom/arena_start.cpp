#include "ScriptPCH.h"
#include "Battleground.h"

class arena_start_early : public GameObjectScript
{
    public:
  	arena_start_early() : GameObjectScript("arena_start_early") {}

        bool OnGossipHello(Player* player, GameObject* go)
        {
			char const* one = ChatHandler(player->GetSession()).GetTrinityString(15012);
			char const* two = ChatHandler(player->GetSession()).GetTrinityString(15013);
			
			if(player->InArena())
				if(player->GetBattleground()->GetStatus() != STATUS_IN_PROGRESS)
				{
					if(player->isWantsEarlier())
						player->ADD_GOSSIP_ITEM(0, one, GOSSIP_SENDER_MAIN, 0);
					else
						player->ADD_GOSSIP_ITEM(0, two, GOSSIP_SENDER_MAIN, 1);
					player->SEND_GOSSIP_MENU(1, go->GetGUID());
				}
			//else
			//	{
			//	ChatHandler(player->GetSession()).PSendSysMessage("You are not in an arena match!");
			//		player->CLOSE_GOSSIP_MENU();
			//if(player->IsInArena())
			//	return false;
			//
            //player->SetWantsEarly(true);
			//ChatHandler(player->GetSession()).PSendSysMessage("asdsda");
			//
			//if(player->WantsEarly())
			//
			//
			return true;
        }

		bool OnGossipSelect(Player* player, GameObject* /*go*/, uint32 sender, uint32 action)
		{
			switch(action)
			{
			case 0:
				player->SetWantsEarlier(true);
				break;
			case 1:
				player->SetWantsEarlier(false);
			}
			player->CLOSE_GOSSIP_MENU();
			return true;
		}
};

class arena_start_early_login : public PlayerScript
{
    public:
		arena_start_early_login() : PlayerScript("arena_start_early_login") { }

	void OnLogin(Player* player)
	{
		player->SetWantsEarlier(false);
	}
};

void AddSC_arena_start_early()
{
    new arena_start_early();
	new arena_start_early_login();
}