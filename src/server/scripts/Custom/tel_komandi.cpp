#include "ScriptPCH.h"
#include "Chat.h"

class cs_mall : public CommandScript
{
        public:
                cs_mall() : CommandScript("cs_mall"){}

        ChatCommand * GetCommands() const
        {
                static ChatCommand WorldChatCommandTable[] = 
                {
                        {"mall",        0,                              false,          &HandleMallCommand,     "", NULL},
						{"duelzone",        0,                              false,          &HandleDuelZoneCommand,     "", NULL},
						{"transmogrification",        0,                              false,          &HandleTransmogrificationCommand,     "", NULL},
						{"profession",        0,                              false,          &HandleProfessionCommand,     "", NULL},
                        {NULL,          0,                              false,          NULL,                                           "", NULL}
                };

                return WorldChatCommandTable;
        }

        static bool HandleMallCommand(ChatHandler* handler, const char* /*args*/)
        {
                Player* player = handler->GetSession()->GetPlayer();
                if (player->isInCombat())
                {
                        player->GetSession()->SendNotification("You cannot use this teleport in combat!");
                        return false;
                }

                if (player->GetTeam() == ALLIANCE)
                        player->TeleportTo(0, -8858.962891f, 660.508972f, 96.932205f, 5.330312f);
                else
                        player->TeleportTo(1, 1430.366943f, -4370.148438f, 25.462860f, 4.831445f);
                return true;
        }
		
		static bool HandleDuelZoneCommand(ChatHandler* handler, const char* /*args*/)
        {
                Player* player = handler->GetSession()->GetPlayer();
                if (player->isInCombat())
                {
                        player->GetSession()->SendNotification("You cannot use this teleport in combat!");
                        return false;
                }

                if (player->GetTeam() == ALLIANCE)
                        player->TeleportTo(1,	5260.790527f,	-2170.427979f,    1259.367554f,	0.786769f);
                else
                        player->TeleportTo(1,	5260.790527f,	-2170.427979f,    1259.367554f,	0.786769f);
                return true;
        }
		
		static bool HandleTransmogrificationCommand(ChatHandler* handler, const char* /*args*/)
        {
                Player* player = handler->GetSession()->GetPlayer();
                if (player->isInCombat())
                {
                        player->GetSession()->SendNotification("You cannot use this teleport in combat!");
                        return false;
                }

                if (player->GetTeam() == ALLIANCE)
                        player->TeleportTo(530, -1734.208984f, 5818.268555f, 148.657898f, 1.243228f);
                else
                        player->TeleportTo(530, -1734.208984f, 5818.268555f, 148.657898f, 1.243228f);
                return true;
        }
		
		static bool HandleProfessionCommand(ChatHandler* handler, const char* /*args*/)
        {
                Player* player = handler->GetSession()->GetPlayer();
                if (player->isInCombat())
                {
                        player->GetSession()->SendNotification("You cannot use this teleport in combat!");
                        return false;
                }

                if (player->GetTeam() == ALLIANCE)
                        player->TeleportTo(1,	-2653.291260f,	-5052.671875f,	23.063002f,	0.777204f);
                else
                        player->TeleportTo(1,	-2653.291260f,	-5052.671875f,	23.063002f,	0.777204f);
                return true;
        }
};

void AddSC_cs_mall()
{
        new cs_mall();
}