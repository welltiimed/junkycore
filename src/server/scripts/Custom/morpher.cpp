#include "ScriptPCH.h"

class npc_morph : public CreatureScript
{
public:
    npc_morph() : CreatureScript("npc_morph") { }

    bool OnGossipHello(Player *player, Creature *_creature)
    {
        if (player->isInCombat())
        {
            player->CLOSE_GOSSIP_MENU();
            _creature->MonsterWhisper("Combat!", player->GetGUID());
            return true;
        }
        else
        {
            player->ADD_GOSSIP_ITEM( 3, "|cff667700Gnome"                    , GOSSIP_SENDER_MAIN, 1);
            player->ADD_GOSSIP_ITEM( 3, "|cff667700Human"                    , GOSSIP_SENDER_MAIN, 2);
            player->ADD_GOSSIP_ITEM( 3, "|cff667700Tauren"                   , GOSSIP_SENDER_MAIN, 3);
             player->ADD_GOSSIP_ITEM( 3, "|cff667700Goblin"                   , GOSSIP_SENDER_MAIN, 4);
             player->ADD_GOSSIP_ITEM( 3, "|cff667700Blood Elf"                , GOSSIP_SENDER_MAIN, 5);
             player->ADD_GOSSIP_ITEM( 3, "|cff667700Old Draenei"                   , GOSSIP_SENDER_MAIN, 6);
             player->ADD_GOSSIP_ITEM( 3, "|cff667700Other"                   , GOSSIP_SENDER_MAIN, 7);
             player->ADD_GOSSIP_ITEM( 9, "|cff667700Demorph"                  , GOSSIP_SENDER_MAIN, 8);
            
        }

        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
        return true;
    }

    bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 uiAction)
    {
        if (sender == GOSSIP_SENDER_MAIN)
        {
            player->PlayerTalkClass->ClearMenus();
            switch(uiAction)
            {
             case 1://Gnome
                        player->ADD_GOSSIP_ITEM( 3, "|cff3300FFMale"                      , GOSSIP_SENDER_MAIN, 9);
                        player->ADD_GOSSIP_ITEM( 3, "|cffFF00EEFemale"                    , GOSSIP_SENDER_MAIN, 10);

                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
                        break;

             case 2://Human
                        player->ADD_GOSSIP_ITEM( 3, "|cff3300FFMale"                      , GOSSIP_SENDER_MAIN, 11);

                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
                        break;

             case 3://Tauren
                        player->ADD_GOSSIP_ITEM( 3, "|cff3300FFMale"                      , GOSSIP_SENDER_MAIN, 12);
                        player->ADD_GOSSIP_ITEM( 3, "|cffFF00EEFemale"                    , GOSSIP_SENDER_MAIN, 13);

                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
                        break;

             case 4://Goblin
                        player->ADD_GOSSIP_ITEM( 3, "|cff3300FFMale"                      , GOSSIP_SENDER_MAIN, 14);
                        player->ADD_GOSSIP_ITEM( 3, "|cffFF00EEFemale"                    , GOSSIP_SENDER_MAIN, 15);

                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
                        break;

             case 5://Blood Elf
                        player->ADD_GOSSIP_ITEM( 3, "|cff3300FFMale"                      , GOSSIP_SENDER_MAIN, 16);
                        player->ADD_GOSSIP_ITEM( 3, "|cffFF00EEFemale"                    , GOSSIP_SENDER_MAIN, 17);

                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
                        break;

             case 6://Broken
                        player->ADD_GOSSIP_ITEM( 3, "|cff3300FFMale"                      , GOSSIP_SENDER_MAIN, 18);

                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
                        break;

             case 7://Other
                        player->ADD_GOSSIP_ITEM( 3, "|cff667700Random NPC's"                         , GOSSIP_SENDER_MAIN, 20);
                        player->ADD_GOSSIP_ITEM( 3, "|cff667700Pirates"                            , GOSSIP_SENDER_MAIN, 21);
                        player->ADD_GOSSIP_ITEM( 3, "|cff667700The Lich King"                            , GOSSIP_SENDER_MAIN, 22);
                        player->ADD_GOSSIP_ITEM( 3, "|cff667700Ghost"                             , GOSSIP_SENDER_MAIN, 23);
                        player->ADD_GOSSIP_ITEM( 3, "|cff667700Wolf"                              , GOSSIP_SENDER_MAIN, 24);

                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
                        break;
              case 20://Champion
                       player->ADD_GOSSIP_ITEM( 3, "|cff3300FFPrince Valanar"                        , GOSSIP_SENDER_MAIN, 25);
                       player->ADD_GOSSIP_ITEM( 3, "|cff3300FFIlidan NerdRage"                        , GOSSIP_SENDER_MAIN, 27);
                       player->ADD_GOSSIP_ITEM( 3, "|cff3300FFFel Orc"                        , GOSSIP_SENDER_MAIN, 28);
                       player->ADD_GOSSIP_ITEM( 3, "|cff3300FFKing Varian"                        , GOSSIP_SENDER_MAIN, 30);
                       player->ADD_GOSSIP_ITEM( 3, "|cff3300FFThrall"                           , GOSSIP_SENDER_MAIN, 31);
                       player->ADD_GOSSIP_ITEM( 3, "|cff3300FFLady Sylvanas"                        , GOSSIP_SENDER_MAIN, 32);
                       player->ADD_GOSSIP_ITEM( 3, "|cff3300FFJaina Proudmore"                    , GOSSIP_SENDER_MAIN, 33);
                       player->ADD_GOSSIP_ITEM( 3, "|cff3300FFDeathbringer Saurfang"                               , GOSSIP_SENDER_MAIN, 34);

                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
                        break;
              case 21://Pirate
                        player->ADD_GOSSIP_ITEM( 3, "|cff3300FFMale"                               , GOSSIP_SENDER_MAIN, 35);
                        player->ADD_GOSSIP_ITEM( 3, "|cffFF00EEFemale"                            , GOSSIP_SENDER_MAIN, 36);

                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
                        break;
      case 9://Gnome - Male
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(20580);
      break;

      case 10://Gnome - Female
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(20581);
      break;

      case 11://Human - Male
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(19723);
      break;

      case 12://Tauren - Male
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(20585);
      break;

      case 13://Tauren - Female
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(20584);
      break;

      case 14://Goblin - Male
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(7111);
      break;

      case 15://Goblin - Female
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(20583);
      break;

      case 16://Blood Elf - Male
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(20578);
      break;

      case 17://Blood Elf - Female
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(20579);
      break;

      case 18://Broken - Male
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(21105);
      break;

      case 22://Arthas
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(22234);
      break;

      case 23://Ghost
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(14560);
      break;

      case 24://Wolf
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(741);
      break;

      case 35://Pirate - Male
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(4620);
      break;

      case 36://Pirate - Female
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(4619);
      break;

      case 25://Valanar
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(30858);
      break;

      case 27://Ilidan
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(27571);
      break;

      case 28://Fel Orc
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(21153);
      break;

      case 30://Varian
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(28127);
      break;

      case 31://Thrall
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(4527);
      break;

      case 32://Sylvanas
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(28213);
      break;

      case 33://Jaina
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(30867);
      break;

      case 34://Saurfang
          player->CLOSE_GOSSIP_MENU();
          player->SetDisplayId(30790);
      break;

       case 8://Demorph
          player->CLOSE_GOSSIP_MENU();
          player->DeMorph();
       break;
             default:
        break;                   
    }
   }
  return true;
 }
};

void AddSC_npc_morph()
{
    new npc_morph();
}